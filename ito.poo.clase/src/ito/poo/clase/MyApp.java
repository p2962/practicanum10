package ito.poo.clase;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.time.LocalDate;
import java.util.ArrayList;
import ito.poo.clase.Cuenta;
import ito.poo.clase.Persistencia;
import ito.poo.clase.ArchivoTexto;


public class MyApp {
	public static void main(String[] args) {
		Scanner sn = new Scanner(System.in);
		Cuenta cuent= new Cuenta(0, null, null, 0, null);
		ArrayList<Cuenta>cu=new ArrayList<Cuenta>();
        boolean salir = false;
        int opcion;
 
        while (!salir) {
 
        	System.out.println("=========Banco==========\n");
            System.out.println("1. Agregar cuenta");
            System.out.println("2. Listar cuentas");
            System.out.println("3. Hacer retiro de una cuenta");
            System.out.println("4. Hacer deposito en una cuenta");
            System.out.println("5. Salir");
 
            try {
 
                System.out.println("Escribe una de las opciones");
                opcion = sn.nextInt();
 
                switch (opcion) {
                    case 1:
                        System.out.println("Agrego una cuentas");
                        cu.add(new Cuenta(1,"Angel",LocalDate.of(2020, 10, 13),0.0f,LocalDate.of(2020, 10, 13)));
                		cu.add(new Cuenta(2,"Elias",LocalDate.of(2021, 12, 10),0.0f,LocalDate.of(2021, 12, 10)));
                		cu.add(new Cuenta(3,"Alan",LocalDate.of(2019, 5, 18),0.0f,LocalDate.of(2019, 5, 18)));
                		try {
                			   ArchivoTexto.grabaDatos(cu);
                			}catch(Exception e) {
                				System.err.println(e.getMessage());
                			}
                			ArrayList<Cuenta> l=null;
                			
                			try {
                				l=ArchivoTexto.leerDatos();
                			}catch(Exception e) {
                				
                			}
                			
                			for(Cuenta cu1: l)
                				System.out.println(cu1);
                			try {
                				   Persistencia.grabaDatos(cu);
                				}catch(Exception e) {
                					System.err.println(e.getMessage());
                				}
                			ArrayList<Cuenta> cu1=null;
                			try {
                				cu1= Persistencia.leeDatos();
                			}catch(Exception e) {
                				
                			}
                			
                			for(Cuenta i: cu1)
                				System.out.println(i);
                        break;
                    case 2:
                        System.out.println("Cuentas:");
                        System.out.println(cu);
                        break;
                    case 3:
                        System.out.println("Retira aqui:");
                        cuent.retiro(100);
                        break;
                    case 4:
                        System.out.println("Deposita aqui:");
                        cuent.deposito(100);;
                        break;
                    case 5:
                    	System.out.println("Adis");
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo nmeros entre 1 y 5");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un nmero");
                sn.next();
                sn.close();
            }
        }

	}
}
